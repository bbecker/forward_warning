/*
 * Copyright (c) 2023 bbecker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

rcmail.addEventListener('init', function(evt) {
    console.log(evt);
    window.redirect_warning_shown = false;

    const show_warning = function() {
        if(!window.redirect_warning_shown) {
            let ac = document.getElementById("actions");
            let warn = document.createElement("p");
            warn.style.padding = "1em"
            warn.style.background = "rgba(125, 35, 35, .5)";
            warn.style.border = "2px solid rgba(140, 42, 42, 0.91)";
            warn.style.borderRadius = ".3em";
            warn.style.fontWeight = 600;

            warn.innerText = "Warning! You should not forward all mail, from all senders to an external inbox. Especially with freemail providers like Google or Microsoft, this violates our and MPG's TOS as well as european privacy laws (GDPR) and german civil laws (BGB)";
            ac.parentNode.insertBefore(warn, ac);
            window.redirect_warning_shown = true;
        }
    }

    if (evt.action === "plugin.managesieve-action") {
        let i = 0;
        let ae;
        while ((ae = document.getElementById("action_type" + i)) !== null) {
            console.log(ae.value);
            if(ae.value === "redirect") {
                show_warning()
            }
            i++;
        }
    }
    window.__action_type_select = window.action_type_select;
    window.action_type_select = function(id) {
        var obj = document.getElementById('action_type' + id);
        if(obj.value === "redirect") {
            show_warning()
        }
        return window.__action_type_select(id);
    }
});