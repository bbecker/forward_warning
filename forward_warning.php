<?php
// Copyright (c) 2023 Bennet Becker <dev@bennet.cc>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

const VERSION = "1.0";


/** @noinspection PhpUnused */
class forward_warning extends rcube_plugin
{
    private static function log($line): void
    {
        $lines = explode(PHP_EOL, $line);
        rcmail::write_log("fwwarn", "[forward_warning] ".$lines[0]);
        unset($lines[0]);
        if (count($lines) > 0) {
            foreach ($lines as $l) {
                rcmail::write_log("fwwarn", str_pad("...",strlen("[forward_warning] "), " ", STR_PAD_BOTH).$l);
            }
        }
    }

    public function init(): void
    {
        $rcmail = rcmail::get_instance();
        $this->load_config();

        $this->add_hook("ready", function ($param) {
            if ($param["action"] == "plugin.managesieve-action") {
                $this->include_script("client.js");
            }
        });
    }
}
